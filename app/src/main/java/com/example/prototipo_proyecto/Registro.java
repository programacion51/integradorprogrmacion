package com.example.prototipo_proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Registro extends AppCompatActivity {


    Button VolverLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        VolverLog= (Button) findViewById(R.id.btnVolverLogin);

        // metodos para ejecutar los botones mediante un click
        VolverLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Volver = new Intent (Registro.this,MainActivity.class);
                startActivity(Volver);
            }
        });
    }
}