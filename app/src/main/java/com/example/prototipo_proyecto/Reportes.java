package com.example.prototipo_proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Reportes extends AppCompatActivity {

    Button Volvermenuprincipal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportes);

        Volvermenuprincipal= (Button) findViewById(R.id.btnVolver6);


        // metodos para ejecutar los botones mediante un click

        Volvermenuprincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent VOLVER= new Intent (Reportes.this,MenuPrincipal.class);
                startActivity(VOLVER);
            }
        });
    }
}