package com.example.prototipo_proyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuPrincipal extends AppCompatActivity {

    Button b1,b2,b3,b4,b5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        b1= (Button) findViewById(R.id.btnMascotas);
        b2= (Button) findViewById(R.id.btnAccesorios);
        b3= (Button) findViewById(R.id.btnVehiculos);
        b4= (Button) findViewById(R.id.btnChatcomunitario);
        b5= (Button) findViewById(R.id.btnReportes);


        // metodos para ejecutar los botones mediante un click

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir= new Intent (MenuPrincipal.this,RegistroMascotas.class);
                startActivity(ir);
            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir= new Intent (MenuPrincipal.this,Registro_Accesorios.class);
                startActivity(ir);
            }
        });


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir= new Intent (MenuPrincipal.this,Registro_Vehiculos.class);
                startActivity(ir);
            }
        });


        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir= new Intent (MenuPrincipal.this,Chat_Comunitario.class);
                startActivity(ir);
            }
        });


        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ir= new Intent (MenuPrincipal.this,Reportes.class);
                startActivity(ir);
            }
        });




    }
}